import System.CPUTime
import Text.Printf

isPrime :: Int -> Bool
isPrime n = null [ x | x <- [2..n - 1], n `mod` x  == 0]

primesSum :: Int  -> Int
primesSum y = sum [x | x <- [2..y], isPrime x == True]

choose n 1 = n
choose n k
    | k > n     = -1
    | k > n - k = choose n  (n - k)
    | otherwise = ((choose n (k - 1)) * (n - k + 1)) `div` k

fac :: Integer -> Integer
fac 0 = 1
fac n = n * fac (n - 1)

test_func1 :: Double -> Double
test_func1 x = fromIntegral(fac(floor(x))) + x^4*sin(-x)/ 10

test_func2 :: Double -> Double
test_func2 x = fromIntegral(primesSum(floor(x))) + x^4*sin(-x)/ 10

test_func3 :: Double -> Double
test_func3 x = fromIntegral(choose (floor(x + 10)) (floor((x + 10)*0.8))) + x^4*sin(-x)/ 10

main :: IO ()
main = do
    start <- getCPUTime
    let min_number = 1
    let max_number = 8
    let points = 50

    let step = ((max_number - min_number) / (points - 1))
    let grid =  bruteforce min_number step max_number
    print $ collector grid (0,0)
    end   <- getCPUTime
    let diff = (fromIntegral (end - start)) / (10^12)
    printf "Seconds: %.9f\n" (diff::Double)

bruteforce  :: Double -> Double -> Double -> [(Double, Double)]
bruteforce current step max_number = 
    if current < max_number then
        [(current, f current)] ++ (bruteforce (current + step) step max_number)
    else 
        []

collector :: [(Double, Double)] -> (Double, Double)  -> (Double, Double)
collector [] (goalX, goalResponse) = (goalX, goalResponse)
collector ((x, response):xs) (goalX, goalResponse) = 
    if goalResponse < response then
        collector xs (x, response)
    else
        collector xs (goalX, goalResponse)
        
f :: Double -> Double
f x = test_func3 x