import System.IO
import Control.Concurrent
import System.CPUTime
import Text.Printf

isPrime :: Int -> Bool
isPrime n = null [ x | x <- [2..n - 1], n `mod` x  == 0]

primesSum :: Int  -> Int
primesSum y = sum [x | x <- [2..y], isPrime x == True]

choose n 1 = n
choose n k
    | k > n     = -1
    | k > n - k = choose n  (n - k)
    | otherwise = ((choose n (k - 1)) * (n - k + 1)) `div` k

fac :: Integer -> Integer
fac 0 = 1
fac n = n * fac (n - 1)

test_func1 :: Double -> Double
test_func1 x = fromIntegral(fac(floor(x))) + x^4*sin(-x)/ 10

test_func2 :: Double -> Double
test_func2 x = fromIntegral(primesSum(floor(x))) + x^4*sin(-x)/ 10

test_func3 :: Double -> Double
test_func3 x = fromIntegral(choose (floor(x + 10)) (floor((x + 10)*0.8))) + x^4*sin(-x)/ 10

collector :: [(Double, Double)] -> (Double, Double)  -> (Double, Double)
collector [] (goalX, goalResponse) = (goalX, goalResponse)
collector ((x, response):xs) (goalX, goalResponse) = 
    if goalResponse < response then
        collector xs (x, response)
    else
        collector xs (goalX, goalResponse)

f :: Double -> MVar Double -> IO()
f x mVar = do 
  putMVar mVar $! test_func3 x
makeStrict (a, b) = seq a (seq b (a, b))

threadHello :: Chan (Double, Double) -> Double -> IO ()
threadHello results x = do
  mVar <- newEmptyMVar
  forkIO $ f x mVar
  response <- takeMVar mVar
  writeChan results (x, response)

truncate' :: Double -> Integer -> Double
truncate' num sg = (fromIntegral . round $ num * f) / f
    where f = 10^sg


main :: IO ()
main = do
  -- disable buffering on stdout
  hSetBuffering stdout NoBuffering
  start <- getCPUTime

  -- constants
  let min_number = 1
  let max_number = 8
  let points = 50
  let step = truncate' ((max_number - min_number) / (points - 1)) 6

  -- Init results channel
  resultsCh <- newChan
  mapM_ (\x -> forkIO $ threadHello resultsCh x) [min_number,(min_number+step)..(max_number-step)] 
  results <- mapM (\_ -> readChan resultsCh) [min_number,(min_number+step)..(max_number -step)] 
  print $ collector results (0,0)
  end   <- getCPUTime
  let diff = (fromIntegral (end - start)) / (10^12)
  printf "Seconds: %.9f\n" (diff::Double)