-module(common).
-export([start/0]).

-import(lists,[sum/1]).

-define(MIN, 1).
-define(MAX, 8).
-define(POINTS, 50).
-define(WORKERS, 10).

fac(1.0) -> 1;
fac(N) -> N * fac(N - 1).

float_round(X) when is_float(X) -> erlang:list_to_float(erlang:float_to_list(X,[{decimals,6}]));
float_round(X) when is_integer(X) -> X.


f(X) -> test_func3(X).

test_func1(X) -> float_round(fac(math:floor(X)) + math:pow(X, 4)*math:sin(-X/10)).
test_func2(X) -> float_round(sum_simple(trunc(X)) + math:pow(X, 2)*math:sin(-X/10)).
test_func3(X) -> float_round(c(trunc(X),trunc(X*0.8)) + math:pow(X, 2)*math:sin(-X/10)).

%% prime function
prime(2)->true;
prime(N)when N rem 2 =:= 0-> false;
prime(3)->true;
prime(Odd)->prime(Odd,3).

prime(N,I)when N rem I =:= 0->false;
prime(N,I)when I*I > N->true;
prime(N,I)->prime(N,I+2).

primes(Start,Finish)->[X|| X <- lists:seq(Start,Finish), prime(X)].
sum_simple(X) ->  sum(primes(2,X)).

c(N, K) when K>0 -> N*c(N-1,K-1)/K;                                         
c(N, 0) -> N.
% int sum_simple(int n) {
% 	int sum = 1;
% 	int *a = new int[n + 1];
% 	for (int i = 0; i < n + 1; i++)
% 		a[i] = i;
% 	for (int p = 2; p < n + 1; p++)	{
% 		if (a[p] != 0) {
% 			sum += a[p];
% 			for (int j = p*p; j < n + 1; j += p)
% 				a[j] = 0;
% 		}
% 	}
% 	return sum;
% }

% void test_funct2(double x, double & v) {
% 	v = sum_simple(int(x)) + pow(x, 2) * sin(-x / 10);
% }

% int C(int n, int k) {
%     if (k == 0 || k == n)
%         return 1;
%     return C(n - 1, k - 1) * n / k;
% }

% void test_funct3(double x, double & v) {
% 	v = C(int(x), int(0.8 * x))	+ pow(x, 2) * sin(-x / 10);
% }

concat(L,[]) ->
   L;
concat(L,[H|T]) ->
   concat(L ++ [H],T).

bruteforce(MAX_RESPONSE, MAX_X, CURRENT, STEP) when CURRENT > ?MAX ->
    io:fwrite("Max point is: ~p", [MAX_RESPONSE]);
bruteforce(MAX_RESPONSE, MAX_X, CURRENT, STEP) when CURRENT =< ?MAX ->
    bruteforce(max(f(CURRENT),MAX_RESPONSE), 0, CURRENT + STEP, STEP).

start() -> Start = os:timestamp(),
  bruteforce(f(3), 0.0, ?MIN, float_round((?MAX - ?MIN) / (?POINTS - 1))),
  io:format("\ntotal time taken ~f seconds~n", [timer:now_diff(os:timestamp(), Start) / 1000000]).
