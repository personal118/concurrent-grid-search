package main

import (
	"log"
	"math"
	"sync"
	"time"
)

func roundFloat(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return float64(math.Round(float64(val)*ratio) / ratio)
}

func f(x float64) float64 {
	return test_funct3(x)
}

type response struct {
	x float64
	y float64
}

func test_funct1(x float64) float64 {
	fac := int64(1)
	for i := int64(1); i <= int64(x); i += 1 {
		fac *= i
	}
	return float64(fac) + float64(math.Pow(float64(x), 4)*math.Sin(float64(-x)/10))
}

func sum_simple(n int64) int64 {
	sum := int64(1)
	a := make([]int64, 0, n+1)
	for i := int64(0); i < n+1; i++ {
		a = append(a, i)
	}
	for p := int64(2); p < n+1; p++ {
		if a[p] != 0 {
			sum += a[p]
			for j := p * p; j < n+1; j += p {
				a[j] = 0
			}
		}
	}
	return sum
}

func test_funct2(x float64) float64 {
	return float64(sum_simple(int64(x))) + math.Pow(x, 2)*math.Sin(-x/10)
}

func C(n int64, k int64) int64 {
	if k == 0 || k == n {
		return int64(1)
	}
	return C(n-1, k-1) * n / k
}

func test_funct3(x float64) float64 {
	return float64(C(int64(x), int64(0.8*x))) + math.Pow(x, 2)*math.Sin(-x/10)
}

func main() {
	now := time.Now()
	min := float64(1)
	max := float64(8)
	points := float64(50)
	step := roundFloat((max-min)/(points-1), 6)

	responseCh := make(chan response, int64(points))

	goalResponse := response{
		y: float64(-10000),
	}
	var wg sync.WaitGroup
	var wg2 sync.WaitGroup
	wg2.Add(1)
	go func() {
		defer wg2.Done()
		for response := range responseCh {
			if response.y > goalResponse.y {
				goalResponse = response
				log.Printf("Max was updated to %v", goalResponse)
			}
		}
	}()

	for x := min; x <= max; x += step {
		x = roundFloat(x, 6)
		wg.Add(1)
		go func(x float64) {
			defer wg.Done()

			r := response{
				x: x,
				y: roundFloat(f(x), 6),
			}

			responseCh <- r
		}(x)
	}

	wg.Wait()
	close(responseCh)
	wg2.Wait()

	log.Printf("Max point is %v", goalResponse)
	log.Printf("Time execution %v", time.Now().Sub(now).Seconds())
}
